export default () => ({
  activeCharacter: null,
  activeObstacle: null,
  characterCardUrl: null,
  defeatedObstacles: [],
  followers: {},
  isDebugMode: false,
  mapData: null
})
