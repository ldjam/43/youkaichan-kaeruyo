import {
  Bodies,
  Body,
  Composite,
  Engine,
  Events,
  Render,
  World
} from 'matter-js'
import PIXI from '~/vendor/pixi'
import characterList from '~/static/characters/index.json'
import obstacleList from '~/static/obstacles.json'

let engine
let debugRenderer
let pixi
const worldObjects = {}
let playerOnGround = false
let playerWalking = false
let characterLayer
let cameraView
let animationTicker

const COLLISION_MASK_DEFAULT = 0x0001
const COLLISION_MASK_FOLLOWER = 0x0002

const distance = (first, second) =>
  Math.sqrt((first.x - second.x) * (first.y - second.y))

const getCharacterIds = state => {
  const { activeCharacter, followers } = state
  return Object.entries(followers)
    .filter(([key, value]) => value)
    .map(([id]) => id)
    .concat(activeCharacter)
}

const isBlocked = layer => {
  const { properties } = layer
  if (!properties) {
    return false
  }

  // Tiled 1.2
  if (Array.isArray(properties)) {
    const property = properties.find(p => p.name === 'blocked')
    return property && property.value
  }

  // Tiled 1.0
  return properties.blocked
}

const addLayer = ({ tilesets, layer }) => {
  const { data, height, name, width, properties } = layer
  const displayObject = new PIXI.Container()
  const physicsBody = isBlocked(layer) ? Composite.create() : null

  for (let row = 0; row < height; row++) {
    for (let column = 0; column < width; column++) {
      const tileId = data[row * width + column]
      if (tileId === 0) {
        continue
      }

      const tileset = tilesets.find(
        t => t.firstgid <= tileId && tileId < t.firstgid + t.tilecount
      )

      const baseTexture = PIXI.BaseTexture.from(tileset.image)
      const { tileheight, tilewidth, columns, firstgid } = tileset
      const frame = new PIXI.Rectangle(
        ((tileId - firstgid) % columns) * tilewidth,
        Math.floor((tileId - firstgid) / columns) * tileheight,
        tilewidth,
        tileheight
      )
      const texture = new PIXI.Texture(baseTexture, frame)
      const sprite = PIXI.Sprite.from(texture)
      sprite.x = column * tilewidth
      sprite.y = row * tileheight
      sprite.anchor.set(0.5)
      displayObject.addChild(sprite)

      if (isBlocked(layer)) {
        const spriteBody = Bodies.rectangle(
          sprite.x,
          sprite.y,
          tilewidth,
          tileheight,
          { isStatic: true }
        )
        spriteBody.position = sprite

        Composite.add(physicsBody, spriteBody)
      }
    }
  }

  addToWorld({
    id: `layer-${name}-${new Date().toISOString()}`,
    name,
    displayObject,
    physicsBody
  })
}

const addToWorld = object => {
  const { id, displayObject, physicsBody, isCharacter } = object
  if (!id) {
    throw new Error('You forgot to specify an id!')
  }

  removeExistingObject(object)

  if (displayObject) {
    displayObject.name = id

    const parentContainer = isCharacter ? characterLayer : cameraView
    parentContainer.addChild(displayObject)
  }

  if (physicsBody) {
    physicsBody.label = id

    World.add(engine.world, physicsBody)
  }

  worldObjects[id] = object
}

const isHiddenObstacleLayer = layer => /^obstacle_.*_hidden$/.test(layer.name)
const isCharacterLayer = layer =>
  layer.type === 'objectgroup' && layer.name === 'characters'
const isObstacleLayer = layer =>
  layer.type === 'objectgroup' && layer.name === 'obstacles'
const isTileLayer = layer =>
  layer.type === 'tilelayer' && !isHiddenObstacleLayer(layer)

const removeExistingObject = ({ id, isCharacter }) => {
  const object = worldObjects[id]
  if (!object) {
    return
  }

  const { displayObject, physicsBody } = object

  if (displayObject) {
    if (isCharacter) {
      characterLayer.removeChild(displayObject)
    } else {
      cameraView.removeChild(displayObject)
    }
  }

  if (physicsBody) {
    World.remove(engine.world, physicsBody)
  }

  delete worldObjects[id]
}

export default {
  addCharacter({ state, commit, dispatch }, character) {
    const { name, position } = character

    const baseImageUrl = `${this.$router.options.base}/characters`
    const imageUrl = `${baseImageUrl}/${name}_animation-small.png`

    const baseTexture = PIXI.BaseTexture.from(imageUrl)
    const frameCount = 4
    const frameSize = 144
    const anchor = new PIXI.Point(0.5, 0.5)

    const frames = []
    for (let i = 0; i < frameCount; i++) {
      const frame = new PIXI.Rectangle(i * frameSize, 0, frameSize, frameSize)
      const texture = new PIXI.Texture(
        baseTexture,
        frame,
        null,
        null,
        null,
        anchor
      )
      frames.push({ texture, time: 200 })
    }

    const displayObject = new PIXI.extras.AnimatedSprite(frames)
    displayObject.x = position.x
    displayObject.y = position.y

    const radius = (displayObject.width + displayObject.height) / 12
    const physicsBody = Bodies.circle(position.x, position.y, radius)
    physicsBody.frictionAir = 0.04
    physicsBody.frictionStatic = 0.8

    physicsBody.position = displayObject

    const id = `${name}-${new Date().toISOString()}`
    addToWorld({
      isCharacter: true,
      ...character,
      id,
      displayObject,
      physicsBody
    })

    if (!state.activeCharacter) {
      commit('SET_ACTIVE_CHARACTER', id)
      dispatch('showCharacterCard', name)
    }
  },

  addFollower({ commit, state, dispatch }, activeCharacterCollision) {
    const { bodyA, bodyB } = activeCharacterCollision

    if (!bodyA.isStatic && !bodyB.isStatic) {
      bodyA.collisionFilter.mask = COLLISION_MASK_DEFAULT
      bodyB.collisionFilter.category = COLLISION_MASK_FOLLOWER

      const id = bodyB.label
      commit('ADD_FOLLOWER', id)
      const { name } = worldObjects[id]
      dispatch('showCharacterCard', name)
    }
  },

  findObstacles({ state, commit, dispatch }) {
    const { activeCharacter, mapData } = state
    if (!activeCharacter || !mapData) {
      return
    }

    const maxDistance = 128

    const character = worldObjects[activeCharacter]
    const { position } = character.physicsBody

    if (
      state.activeObstacle &&
      distance(state.activeObstacle, position) < maxDistance
    ) {
      // still active
      return
    }

    const obstacleLayer = mapData.layers.find(isObstacleLayer)
    if (!obstacleLayer) {
      return
    }

    const { defeatedObstacles } = state
    const activeObstacle = obstacleLayer.objects.find(
      obstacle =>
        distance(obstacle, position) < maxDistance &&
        !defeatedObstacles.includes(obstacle.name)
    )
    if (activeObstacle) {
      const { options } = obstacleList[activeObstacle.name]
      const countElements = element =>
        getCharacterIds(state).filter(id => {
          const { name } = worldObjects[id]
          const { elements } = characterList[name]
          return elements.includes(element)
        }).length
      const hasEnoughDemons = option =>
        Object.entries(option).every(
          ([key, value]) => countElements(key) >= value
        )
      const availableOptions = options.filter(hasEnoughDemons)
      if (availableOptions.length === 0) {
        console.log(
          `No available option for obstacle ${
            activeObstacle.name
          } --> game over`
        )
        return dispatch('gameOver')
      }

      commit('SET_ACTIVE_OBSTACLE', {
        ...activeObstacle,
        availableOptions
      })
    } else if (state.activeObstacle) {
      commit('SET_ACTIVE_OBSTACLE', null)
    }
  },

  killDemon({ state, commit, dispatch }, id) {
    const { activeCharacter, followers } = state
    if (id === activeCharacter) {
      const remainingFollowers = Object.keys(followers).filter(
        id => followers[id] && id !== activeCharacter
      )

      if (remainingFollowers.length === 0) {
        dispatch('gameOver')
        return
      }

      const index = Math.floor(Math.random() * remainingFollowers.length)
      const newLeader = remainingFollowers[index]
      commit('REMOVE_FOLLOWER', newLeader)
      commit('SET_ACTIVE_CHARACTER', newLeader)
    } else {
      commit('REMOVE_FOLLOWER', id)
    }

    removeExistingObject({ id, isCharacter: true })
  },

  startWalking({ dispatch }, { id, x, y }) {
    if (!playerWalking) {
      playerWalking = true

      animationTicker = setInterval(
        () => dispatch('applyForce', { id, x, y }),
        80
      )
    }
  },

  stopWalking({ dispatch }) {
    playerWalking = false
    clearInterval(animationTicker)
  },

  jump({ dispatch }, { id, x, y }) {
    dispatch('applyForce', { id, x, y })
  },

  applyForce({ state }, { id, x, y }) {
    const object = worldObjects[id]
    if (!object) {
      throw new Error(`Could not find object with id ${id}!`)
    }

    const { physicsBody } = object
    if (!physicsBody) {
      throw new Error(`Object with id ${id} has no physics!`)
    }

    // walk
    if (x !== 0) {
      const limit = playerOnGround ? 1 : 0.7
      Body.applyForce(physicsBody, physicsBody.position, {
        x: x * limit,
        y
      })
    }

    if (playerOnGround) {
      // jump
      if (y < 0) {
        let momentum = 0
        const sign = physicsBody.velocity.x > 0 ? 1 : -1
        if (Math.abs(physicsBody.velocity.x) > 0.4) {
          momentum = (sign * physicsBody.frictionAir) / 2.2
        }

        Body.applyForce(physicsBody, physicsBody.position, {
          x: momentum,
          y
        })
      }
    }
  },

  gameOver({ state, commit }) {
    commit('SET_ACTIVE_CHARACTER', null)
    pixi.ticker.destroy()
    state.followers = {}
    this.$router.replace({ name: 'game-over' })
  },

  createWorld({ state, dispatch }, element) {
    const { clientHeight: height, clientWidth: width } = element

    engine = Engine.create()
    pixi = new PIXI.Application({
      width,
      height,
      transparent: true
    })
    element.appendChild(pixi.view)

    cameraView = new PIXI.Container()
    pixi.stage.addChild(cameraView)

    pixi.ticker.add(() => {
      dispatch('stopAnimations')
      dispatch('updateCharacterVitals')
      dispatch('updateFollowerPositions')
      dispatch('updateCameraPosition')
      dispatch('findObstacles')
    })

    characterLayer = new PIXI.Container()
    playerOnGround = false
    playerWalking = false

    Events.off(engine, 'collisionStart')
    Events.off(engine, 'collisionActive')
    Events.off(engine, 'collisionEnd')

    Events.on(engine, 'collisionStart', event => {
      const pairs = event.pairs
      const activeCharacterCollision = pairs.find(
        pair => pair.bodyA.label === state.activeCharacter
      )

      if (activeCharacterCollision) {
        dispatch('addFollower', activeCharacterCollision)
      }
    })

    Events.on(engine, 'collisionActive', event => {
      const pairs = event.pairs

      const activeCharacter = worldObjects[state.activeCharacter]
      if (!activeCharacter) {
        return
      }
      const { displayObject } = activeCharacter

      const activeCharacterCollision = pairs.find(pair =>
        [pair.bodyA.label, pair.bodyB.label].includes(state.activeCharacter)
      )

      if (activeCharacterCollision) {
        playerOnGround = true

        if (!displayObject.playing && activeCharacterCollision.bodyB.isStatic) {
          displayObject.gotoAndPlay(1)
        }
      }
    })

    Events.on(engine, 'collisionEnd', event => {
      const activeCharacter = worldObjects[state.activeCharacter]

      if (!activeCharacter) {
        return
      }

      const { displayObject } = activeCharacter

      playerOnGround = false
      setTimeout(() => {
        if (!playerOnGround) {
          displayObject.gotoAndStop(1)
        }
      }, 50)
    })

    Engine.run(engine)

    debugRenderer = Render.create({
      element,
      engine,
      options: {
        background: 'transparent',
        wireframeBackground: 'transparent',
        height,
        width
      },
      hasBounds: true,
      bounds: { min: { x: 0, y: 0 }, max: { x: width, y: height } }
    })

    Render.run(debugRenderer)
    if (!state.isDebugMode) {
      const { classList } = debugRenderer.canvas
      classList.add('hidden')
    }
  },

  loadMap({ commit, dispatch }, filePath) {
    commit('SET_MAP_DATA', null)
    Object.values(worldObjects)
      .filter(object => !object.isCharacter)
      .forEach(removeExistingObject)

    return this.$axios.$get(`/maps/${filePath}.json`).then(mapData => {
      const baseUrl = `${this.$router.options.base}/maps/${this.filePath}/..`
      const { tilesets } = mapData
      tilesets.forEach(tileset => {
        tileset.image = `${baseUrl}/${tileset.image}`
      })

      commit('SET_MAP_DATA', mapData)

      const { layers } = mapData
      const characterLayerIndex = layers.findIndex(isCharacterLayer)
      const backgroundLayers = layers
        .slice(0, characterLayerIndex)
        .filter(isTileLayer)
      const foregroundLayers = layers
        .slice(characterLayerIndex + 1)
        .filter(isTileLayer)

      backgroundLayers.forEach(layer => addLayer({ tilesets, layer }))
      addToWorld({ id: 'characters', displayObject: characterLayer })
      foregroundLayers.forEach(layer => addLayer({ tilesets, layer }))

      const spawnPoints = layers[characterLayerIndex].objects
      const characters = spawnPoints.map(({ name: characterElement, x, y }) => {
        const matchingCharacters = Object.keys(characterList).filter(name =>
          characterList[name].elements.includes(characterElement)
        )
        const name =
          matchingCharacters[
            Math.floor(Math.random() * matchingCharacters.length)
          ]
        return { name, position: { x, y } }
      })

      characters.forEach(character => dispatch('addCharacter', character))
    })
  },

  setWalkingDirection({ state, commit }, direction) {
    const characters = Object.values(worldObjects).filter(o => o.isCharacter)
    characters.forEach(character => {
      const { displayObject } = character

      if (direction.x !== 0) {
        displayObject.scale.x = -direction.x
        if (!displayObject.playing) {
          displayObject.gotoAndPlay(0)
        }
      } else {
        displayObject.gotoAndStop(1)
      }
    })
  },

  updateFollowerPositions({ dispatch, state }) {
    const active = worldObjects[state.activeCharacter]
    if (!active) {
      return
    }

    Object.keys(state.followers).forEach(id => {
      const follower = worldObjects[id]
      if (!follower) {
        return
      }

      const targetX = active.physicsBody.position.x
      const x = follower.physicsBody.position.x
      const diff = targetX - x

      if (Math.abs(diff) > 100) {
        dispatch('applyForce', { id, x: 0.00002 * diff, y: 0 })
      }
    })
  },

  showCharacterCard({ commit, dispatch }, characterName) {
    const characterCardUrl =
      characterName &&
      require(`~/static/character-cards/${characterName}-steckbrief.png`)
    commit('SET_CHARACTER_CARD', characterCardUrl)

    if (characterName) {
      setTimeout(() => dispatch('showCharacterCard', null), 3000)
    }
  },

  updateCharacterVitals({ dispatch, state }) {
    if (!state.mapData) {
      return
    }

    const { height, tileheight } = state.mapData
    const MAP_BOTTOM = height * tileheight
    const characters = Object.values(worldObjects).filter(o => o.isCharacter)

    characters.forEach(character => {
      const { physicsBody } = character
      if (physicsBody.position.y > MAP_BOTTOM) {
        dispatch('killDemon', character.id)
      } else if (physicsBody.position.x > 18560) {
        this.$router.push({ name: 'win' })
      }
    })
  },

  stopAnimations() {
    const characters = Object.values(worldObjects).filter(o => o.isCharacter)
    characters.forEach(character => {
      const { displayObject, physicsBody } = character
      if (Math.abs(physicsBody.velocity.x) < 0.4) {
        displayObject.gotoAndStop(1)
      }
    })
  },

  toggleDebugMode({ commit, state }) {
    commit('TOGGLE_DEBUG_MODE')

    const { classList } = debugRenderer.canvas
    if (state.isDebugMode) {
      classList.remove('hidden')
    } else {
      classList.add('hidden')
    }
  },

  updateCameraPosition({ state }) {
    const { activeCharacter, mapData } = state
    if (!activeCharacter || !mapData) {
      return
    }

    const mapWidth = mapData.width * mapData.tilewidth
    const character = worldObjects[activeCharacter]
    const { position } = character.physicsBody
    const { width, height } = pixi.view
    const cameraPosition = {
      x: Math.min(mapWidth - width, Math.max(0, position.x - width / 2)),
      y: position.y - height / 2
    }

    const { bounds } = debugRenderer
    bounds.min = cameraPosition
    bounds.max = {
      x: cameraPosition.x + width,
      y: cameraPosition.y + height
    }

    cameraView.x = -cameraPosition.x
    cameraView.y = -cameraPosition.y
  },

  vaporize({ state, commit, dispatch }, option) {
    const { name: obstacleName } = state.activeObstacle
    commit('SET_ACTIVE_OBSTACLE', null)
    commit('DEFEAT_OBSTACLE', obstacleName)

    Object.values(worldObjects)
      .filter(object => object.name === `obstacle_${obstacleName}_visible`)
      .forEach(removeExistingObject)

    const { tilesets, layers } = state.mapData
    const hiddenLayers = layers.filter(
      layer => layer.name === `obstacle_${obstacleName}_hidden`
    )
    hiddenLayers.forEach(layer => addLayer({ tilesets, layer }))

    const requiredElements = { ...option }
    Object.keys(requiredElements).forEach(element => {
      while (requiredElements[element] > 0) {
        const matchingCharacterId = getCharacterIds(state).find(id => {
          const { name } = worldObjects[id]
          return characterList[name].elements.includes(element)
        })
        if (!matchingCharacterId) {
          console.log(`No matching character for ${element} --> gameOver`)
          return dispatch('gameOver')
        }

        const { name } = worldObjects[matchingCharacterId]
        dispatch('killDemon', matchingCharacterId)
        characterList[name].elements.forEach(e => {
          requiredElements[e] -= 1
        })
      }
    })
  }
}
