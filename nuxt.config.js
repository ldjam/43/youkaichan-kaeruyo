const pkg = require('./package')

const baseUrl = '/43/youkaichan-kaeruyo/'

/*
** Axios module configuration
*/
const axios = {
  // See https://github.com/nuxt-community/axios-module#options
  prefix: baseUrl
}

if (process.env.NODE_ENV === 'production') {
  axios.https = true
  axios.host = 'ldjam.gitlab.io'
  axios.port = '443'
}

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Customize the generated output folder
  */
  generate: {
    dir: 'public'
  },

  /*
  ** Customize the base url
  */
  router: {
    base: baseUrl
  },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios'
  ],
  axios
}
