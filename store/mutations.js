export default {
  DEFEAT_OBSTACLE(state, obstacleName) {
    const defeatedObstacles = [...state.defeatedObstacles, obstacleName]
    Object.assign(state, { defeatedObstacles })
  },

  SET_ACTIVE_CHARACTER(state, activeCharacter) {
    Object.assign(state, { activeCharacter })
  },

  SET_ACTIVE_OBSTACLE(state, activeObstacle) {
    Object.assign(state, { activeObstacle })
  },

  SET_CHARACTER_CARD(state, characterCardUrl) {
    Object.assign(state, { characterCardUrl })
  },

  ADD_FOLLOWER(state, follower) {
    state.followers[follower] = true
  },

  REMOVE_FOLLOWER(state, follower) {
    state.followers[follower] = false
  },

  SET_MAP_DATA(state, mapData) {
    Object.assign(state, { mapData })
  },

  TOGGLE_DEBUG_MODE(state) {
    let isDebugMode = !this.state.isDebugMode
    Object.assign(state, { isDebugMode })
  }
}
